#pragma once

#include <iostream>
#include <string>

#include <caller.hpp>
#include <remoteCall.hpp>

#include <local_utils.hpp>

Caller::Return_t fn_loader(std::vector<std::string> _data, void* ptr);