#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

#include <tinyformat.h>

std::string     transformString(std::string input);
std::string     convertNameToPath(const std::string& name);
bool            containsParentTraversal(const std::string& path);
std::string     slurp(std::ifstream& in);
std::string     netIDToString(uint32_t netID);