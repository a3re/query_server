#pragma once

#include <iostream>
#include <string>

#include <nanodbc/nanodbc.h>

#define DB_HOST "127.0.0.1"
#define DB_USER "user"
#define DB_PASS "udADt8GcZ9heTFES4gYQBa"
#define DB_NAME "db_arma"
#define DB_PORT "3306"

typedef struct {
    uint32_t    id;

    std::string discordid;
    std::string nickname;
    std::string steamid;

    union {
        uint32_t    raw_permissions;
        struct {
            uint32_t    is_zeus         : 1;
            uint32_t    is_iventolog    : 1;
            uint32_t    can_throw_out   : 1;
            uint32_t    reserved        : 29;
        };
    };
} DB_Player_t;

bool        db_connect();
DB_Player_t db_get_player_by_steamid(const std::string& steamid);