#pragma once

#include <iostream>
#include <string>

#include <caller.hpp>
#include <remoteCall.hpp>

#include <db.hpp>
#include <local_utils.hpp>

typedef struct {
    uint32_t    netID;
    std::string name;
    std::string steamid;
} FB_CallerInfo_t;

Caller::Return_t fn_func_broker(std::vector<std::string> _data, void* ptr);