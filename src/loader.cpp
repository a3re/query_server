#include <loader.hpp>

#define MODULE_NAME "loader"
#include <log.hpp>

typedef std::function<Caller::Return_t(uint32_t, std::string)> LoaderHandler_t;

Caller::Return_t handleBaseEvent(uint32_t client, std::string event) {
    auto path = "handlers/" + event + ".sqf";

    LOG_INFO("[%s] Handling %s event (path %s)", netIDToString(client), event, path);
    std::ifstream file(path);

    if (file.is_open())
        return {
            .out = slurp(file),
            .ret = Caller::Status_t::OK,
        };

    return {
        .out = tfm::format("Handler for %s not found!", event),
        .ret = Caller::Status_t::INTERNAL_ERROR,
    };
}

Caller::Return_t getFunction(uint32_t client, std::string function) {
    auto path = "functions/" + convertNameToPath(function) + ".sqf";

    if (containsParentTraversal(path))
        return {
            .out = "Potentially malicious path",
            .ret = Caller::Status_t::INTERNAL_ERROR,
        };

    LOG_INFO("[%s] Send code for function %s (path %s)", netIDToString(client), function, path);
    std::ifstream file(path);

    if (file.is_open())
        return {
            .out = slurp(file),
            .ret = Caller::Status_t::OK,
        };

    return {
        .out = tfm::format("Function %s not found!", function),
        .ret = Caller::Status_t::INTERNAL_ERROR,
    };
}

Caller::Return_t getScript(uint32_t client, std::string script) {
    auto path = "scripts/" + convertNameToPath(script) + ".sqf";

    if (containsParentTraversal(path))
        return {
            .out = "Potentially malicious path",
            .ret = Caller::Status_t::INTERNAL_ERROR,
        };

    LOG_INFO("[%s] Send code for script %s (path %s)", netIDToString(client), script, path);
    std::ifstream file(path);

    if (file.is_open())
        return {
            .out = slurp(file),
            .ret = Caller::Status_t::OK,
        };

    return {
        .out = tfm::format("Script %s not found!", script),
        .ret = Caller::Status_t::INTERNAL_ERROR,
    };
}

std::vector<LoaderHandler_t> func_handlers = {
    handleBaseEvent,
    getFunction,
    getScript
};

Caller::Return_t fn_loader(std::vector<std::string> _data, void* ptr) {
    Caller::Return_t error = {
        .out =  "Bad argument",
        .ret =  Caller::Status_t::NOT_ENOUGH_ARGUMENTS,
    };

    if (_data.size() < 3)
        return error;

    uint32_t func_num = std::stoi(_data[0]);

    if (func_num >= func_handlers.size())
        return {
            .out =  "Bad function",
            .ret =  Caller::Status_t::INTERNAL_ERROR,
        };

    uint32_t client = std::stoi(_data[1]);

    return func_handlers[func_num](client, _data[2]);
}