#include <db.hpp>

#define MODULE_NAME "db"
#include <log.hpp>

nanodbc::connection mysql_connection;

bool db_connect() {
    LOG_INFO("Connecting to database...");
    try {
        mysql_connection = nanodbc::connection(
            NANODBC_TEXT("Driver={MySQL ODBC 8.0 Driver};"
                         "Server=" DB_HOST ";"
                         "Port=" DB_PORT ";"
                         "Database=" DB_NAME ";"
                         "User=" DB_USER ";"
                         "Password=" DB_PASS ";")
        );
        LOG_INFO("Connected to database!");
    } catch (const std::exception& e) {
        LOG_INFO("Could not connect to database: %s", e.what());
        return false;
    }
    return true;
}

void db_check_connection() {
    if (!mysql_connection.connected()) {
        LOG_WARN("Detected closed connection to database, reconnecting...");
        db_connect();  // Reconnect if the connection is closed.
    }
}

DB_Player_t db_get_player_by_steamid(const std::string& steamid) {
    DB_Player_t player = {
        .id = UINT32_MAX,
    };

    db_check_connection();

    try {
        nanodbc::statement stmt(mysql_connection);
        nanodbc::prepare(stmt, NANODBC_TEXT("SELECT * FROM players WHERE steamid = ?"));
        stmt.bind(0, steamid.c_str());

        auto res = nanodbc::execute(stmt);

        if (res.next()) {
            player.id               = res.get<uint32_t>("id");
            player.discordid        = res.get<uint32_t>("discordid");
            player.nickname         = res.get<std::string>("nickname");
            player.steamid          = res.get<std::string>("steamid");
            player.raw_permissions  = res.get<uint32_t>("permissions");

            LOG_INFO("Found player %s (%s) in database", player.nickname.c_str(), player.steamid.c_str());
        } else {
            LOG_INFO("Player %s not found in database", steamid.c_str());
        }
    } catch (const std::exception& e) {
        LOG_ERROR("Database error: %s", e.what());
    }

    return player;
}
