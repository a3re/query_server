#include <local_utils.hpp>

std::string transformString(std::string input) {
    std::string result;
    bool inQuotes = false;
    char lastChar = ' ';

    auto orig_len = input.length();
    auto start = input[0] == '"' ? 1 : 0;
    auto len = input[orig_len - 1] == '"' ? orig_len - start - 1 : orig_len;
    
    input = input.substr(start, len);
    
    for (char c : input) {
        if (c == '"') {
            if (lastChar == '"')
                continue;
                
            inQuotes = !inQuotes;
        } else if (c == ' ' && !inQuotes) {
            continue;
        }
        
        lastChar = c;
        result += c;
    }
    
    return result;
}

std::string convertNameToPath(const std::string& name) {
    std::istringstream iss(name);
    std::string segment;
    std::string path;
    std::vector<std::string> segments;

    while (std::getline(iss, segment, '_'))
        segments.push_back(segment);

    for (const auto& s : segments)
        path += s + "/";

    if (!path.empty() && path.back() == '/')
        path.pop_back();

    return path;
}

bool containsParentTraversal(const std::string& path) {
    return path.find("..") != std::string::npos;
}

std::string slurp(std::ifstream& in) {
    std::ostringstream sstr;
    sstr << in.rdbuf();
    return sstr.str();
}

std::string netIDToString(uint32_t netID) {
    return (netID == 2 ? "SERVER" : tfm::format("CLIENT(%d)", netID));
};