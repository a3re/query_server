#include <func_broker.hpp>

#define MODULE_NAME "func_broker"
#include <log.hpp>

#include <sqf-value/value.hpp>

typedef std::function<Caller::Return_t(FB_CallerInfo_t, std::vector<std::string>)> BrokerHandler_t;

static Caller::Return_t _error_nea = {
    .out = "Not enough arguments",
    .ret = Caller::Status_t::NOT_ENOUGH_ARGUMENTS,
};

Caller::Return_t get_player(FB_CallerInfo_t caller, std::vector<std::string> args) {
    if (args.size() < 1)
        return _error_nea;

    DB_Player_t player = db_get_player_by_steamid(args[0]);

    if (player.id != UINT32_MAX) {
        sqf::value answer = {
            (int)player.id,
            player.nickname,
            player.steamid,
            {
                (bool)player.is_zeus,
                (bool)player.is_iventolog,
                (bool)player.can_throw_out,
            },
            player.discordid
        };

        LOG_INFO("[%s] Found player: %s (%s)", netIDToString(caller.netID), player.nickname, player.steamid);

        return {
            .out = answer.to_string(),
            .ret = Caller::Status_t::OK,
        };
    }

    return {
        .out = "Unable to obtain player: Not found",
        .ret = Caller::Status_t::INTERNAL_ERROR,
    };
}

std::vector<std::pair<std::string, BrokerHandler_t>> broker_handlers = {
    #define FUNC(V) { #V, V }

    FUNC(get_player),
};

Caller::Return_t fn_func_broker(std::vector<std::string> _data, void* ptr) {
    if (_data.size() < 4)
        return _error_nea;

    auto it = std::find_if(broker_handlers.begin(), broker_handlers.end(), [_data](auto& pair) {
        return pair.first == _data[0];
    });

    if (it == broker_handlers.end())
        return {
            .out =  "Bad function",
            .ret =  Caller::Status_t::INTERNAL_ERROR,
        };

    FB_CallerInfo_t caller = {
        .netID      = (uint32_t)std::stoi(_data[1]),
        .name       = _data[2],
        .steamid    = _data[3],
    };

    std::vector<std::string> func_args(_data.begin() + 4, _data.end());

    LOG_INFO("[%s:%s] Call function %s", netIDToString(caller.netID), caller.name, it->first);
    return it->second(caller, func_args);
}