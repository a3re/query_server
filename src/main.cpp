#include <iostream>
#include <string>
#include <csignal>
#include <atomic>

#define MODULE_NAME "query_server"
#include <log.hpp>

#include <caller.hpp>
#include <remoteCall.hpp>

#include <db.hpp>

#include <loader.hpp>
#include <func_broker.hpp>

std::atomic<bool> isRunning = true;

Caller* caller;
RemoteCall* rc;

void signalHandler(int signum) {
    if (signum == SIGINT)
        std::cout << "Interrupt signal received (Ctrl+C) (" << signum << ")\n";
    else
        std::cout << "Interrupt signal received (" << signum << ")\n";


    if (signum != SIGSEGV) {
        isRunning = false;

        delete rc;
        delete caller;
    } else {
        std::cout << "!!!!Segmentation fault!!!!\n";
    };

    exit(signum);
}

int main(int argc, char *argv[]) {
    signal(SIGINT, signalHandler);
    signal(SIGTERM, signalHandler);
    signal(SIGSEGV, signalHandler);

    int opt;

    std::string ip = "127.0.0.1";
    int port = 8386;

    for (int i = 1; i < argc; ++i) {
        std::string arg = argv[i];

        if (arg == "-h") {
            std::cout << "Example: ./A3REServer -i <ip> -p <port>\n";
            return 0;
        } else if (arg == "-i") {
            if (i + 1 < argc) {
                ip = argv[++i];
            } else {
                std::cerr << "Option -i requires an argument.\n";
                return EXIT_FAILURE;
            }
        } else if (arg == "-p") {
            if (i + 1 < argc) {
                port = std::atoi(argv[++i]);
            } else {
                std::cerr << "Option -p requires an argument.\n";
                return EXIT_FAILURE;
            }
        } else {
            std::cerr << "Unknown option: " << arg << "\n";
            std::cout << "Example: ./A3REServer -i <ip> -p <port>\n";
            return EXIT_FAILURE;
        }
    }


    UDPNode::node_t server(ip, port);

    caller = new Caller();
    rc = new RemoteCall(caller, Node::type_t::NODE_TYPE_SERVER, server);

    LOG_INFO("Remote call subsystem version: %s", rc->getVersion());

    caller->addFunction("loader", fn_loader);

    if (db_connect())
        caller->addFunction("broker", fn_func_broker);
    else
        LOG_ERROR("Could not connect to database! Broker will not be available!");

    while (isRunning) {
        std::this_thread::yield();
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}